-- SUMMARY --

This module keeps history of variables whenever we need to know assigned value 
in variable. 
This module display a list of variables with their value.
This module is very useful for development we will find all variable value on 
one URL except go on form and check value.

-- REQUIREMENTS --
  No

-- INSTALLATION --

* Install as usual.


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:


-- CUSTOMIZATION --

* Access menu from here : admin/config/development/variable-history

-- CONTACT --

Current maintainers:
* Ved Pareek
  email : vedmona@gmail.com
